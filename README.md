# Seminar Offenburg SS2020  

This is a simple TCP server implementation written for a proof of concept.  
For the poc the tcp server takes a port to listen on and a vpn gateway's dns name.  
Every TCP connection is answered with the ip address of the remote part, wheter the connection is made through a tor exit node or the provided vpn gateway.  
