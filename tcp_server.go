package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	// get port to listen on
	port := flag.Int("p", -1, "Port to listen on")
	hostname := flag.String("h", "", "hostname of vpn server")
	flag.Parse()

	if *port < 0 {
		panic("ERROR no port provided")
	} else if *port <= 1024 && os.Geteuid() != 0 {
		panic("ERROR chosen port only valid if running as root")
	}

	// start tcp server
	tcpAddr, err := net.ResolveTCPAddr("tcp", "0.0.0.0:"+strconv.Itoa(*port))
	if err != nil {
		panic(err)
	}

	tcpListener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		panic(err)
	}
	fmt.Printf("tcp server listening on %d\n", *port)
	fmt.Printf("using vpn endpoint %s\n", *hostname)

	// listen for connection
	for {
		tcpConn, err := tcpListener.AcceptTCP()
		if err != nil {
			panic(err)
		}
		// handle connection async
		go func(c *net.TCPConn) {
			defer c.Close()
			// print remote ip
			ipPort := c.RemoteAddr().String()
			ip := strings.Split(ipPort, ":")
			msg := fmt.Sprintf("your ip address seems to be %s\n", ip[0])
			c.Write([]byte(msg))

			// print tor status
			tor, err := checkTor(ip[0])
			if err {
				msg = fmt.Sprintf("unable to check tor connection\n")
			} else {
				msg = fmt.Sprintf("connected over tor: %t\n", tor)
			}
			c.Write([]byte(msg))

			// print vpn status
			vpn, err := checkVPN(*hostname, ip[0])
			if err {
				msg = fmt.Sprintf("unable to check vpn connection\n")
			} else {
				msg = fmt.Sprintf("connected over vpn: %t\n", vpn)
			}
			c.Write([]byte(msg))

			// print log to console
			t := time.Now().Format("Jan 2 15:04:05 MST")
			fmt.Printf("%s\tconnection from %s; tor: %t; vpn: %t;\n", t, ip[0], tor, vpn)
		}(tcpConn)
	}
}

func checkTor(ip string) (bool, bool) {
	// get tor exit node list
	resp, err := http.Get("https://check.torproject.org/torbulkexitlist")
	if err != nil {
		panic(err)
	}
	if resp.StatusCode != 200 {
		return false, true
	}
	defer resp.Body.Close()

	// check if remote ip is in there
	body, err := ioutil.ReadAll(resp.Body)
	return bytes.Contains(body, []byte(ip)), false
}

func checkVPN(hostname, ip string) (bool, bool) {
	// unable to check empty hostname
	if hostname == "" {
		return false, true
	}

	// resolve hostname to address
	addrs, err := net.LookupHost(hostname)
	if err != nil {
		return false, true
	}

	// check if remote ip is the vpn endpoint
	for _, v := range addrs {
		if strings.Compare(v, ip) == 0 {
			return true, false
		}
	}
	return false, false
}
